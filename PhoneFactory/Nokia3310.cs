﻿using System;

namespace PhoneFactory
{
    public class Nokia3310 : IFeaturePhone
    {
        public decimal Id { get; set; }
        public string Name { get; set; }

        public void MakeCall(decimal number)
        {
            Console.WriteLine($"Calling {number}");
        }

       

        public void PlayMP3(string songName)
        {
            Console.WriteLine("Playing {0}",songName);
        }

        

        public void PowerOn()
        {
            Console.WriteLine("Power is on!");
        }

        public void PowerOff()
        {
             Console.WriteLine("Device is shooting down!" + Environment.NewLine);
        }

        public string GetInformation()
        {
            return Environment.NewLine+
                   $"Id :{Id}" + Environment.NewLine +
                   $"Name :{Name}" + Environment.NewLine +
                   "Internet browsing: No" + Environment.NewLine +
                   "Bluetooth: No" + Environment.NewLine +
                   "MP3: No" + Environment.NewLine +
                   "NFC: No" + Environment.NewLine;
        }
    }
}