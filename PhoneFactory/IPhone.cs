﻿namespace PhoneFactory
{
    public interface IPhone
    {
        
        void AccessInternet();
        void ShareFileOverBluetooth(string fileName);
        void PlayMP3(string songName);
        void ConnectToDeviceViaNFC(string deviceName);
         
    }
}