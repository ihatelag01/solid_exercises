﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneFactory
{
    class MotorolaStarTAC : IFeaturePhone
    {
        public decimal Id { get; set; }
        public string Name { get; set; }

        public void MakeCall(decimal number)
        {
            Console.WriteLine($"Calling {number}");
        }
        public string GetInformation()
        {
            return Environment.NewLine +
                   $"Id :{Id}" + Environment.NewLine +
                   $"Name :{Name}" + Environment.NewLine +
                   "Internet browsing: No" + Environment.NewLine +
                   "Bluetooth: No" + Environment.NewLine +
                   "MP3: Yes" + Environment.NewLine +
                   "NFC: No" + Environment.NewLine;
        }

        public void PowerOn()
        {
            Console.WriteLine("Power is on!");
        }

        public void PowerOff()
        {
            Console.WriteLine("Device is shooting down!" + Environment.NewLine);
        }

        public void PlayMP3(string songName)
        {
            Console.WriteLine($"Playing song {songName}");
        }
    }
}
