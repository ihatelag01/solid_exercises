﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneFactory
{
    interface IFeaturePhone
    {
       
        void MakeCall(decimal number);
        void PowerOn();
        void PowerOff();
        string GetInformation();
        void PlayMP3(string songName);
    }
}
