﻿using System;

namespace UserCreation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to user creation app!");

            var user = new Person();

            user.enterFirstName();

            user.enterLastName();

            user.displayUsername();

            Console.WriteLine("Press enter to exit...");

            Console.ReadLine();
        }
    }
}
