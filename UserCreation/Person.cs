﻿using System;

namespace UserCreation
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public void enterFirstName()
        {
            Console.WriteLine("Enter your first name:");

            FirstName = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(FirstName))
            {
                Console.WriteLine("First name is not valid!");
                Console.ReadLine();
                return;
            }
        }

        public void enterLastName()
        {
            Console.WriteLine("Enter your last name:");

            LastName = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(LastName))
            {
                Console.WriteLine("Last name is not valid!");
                Console.ReadLine();
                return;
            }
        }

        public void displayUsername()
        {
            Console.WriteLine($"Your new username is {FirstName.Substring(0, 2)}{LastName}");
        }
    }
}
