﻿using System.IO;

namespace SmartHouse
{
    public class Security
    {
        public void FullLockDown(FileDatabase _db)
        {
            const string message = "Full LockDown enabled!";

            var db = _db;
            db.Save(message);
        }

        public void LockFrontDoor(FileDatabase _db)
        {
            const string message = "Front door locked";

            var db = _db;
            db.Save(message);
        }

        public string UnlockFrontDoor(decimal accessCode,FileDatabase _db)
        {
            const string validMessage = "Access granted!";
            const string invalidMessage = "Access code Invalid! Access denied!";

            var db = _db;

            if (accessCode != db.GetAccessCode())
            {
                db.Save(invalidMessage);
                return invalidMessage;
            }

            db.Save(validMessage);

            return validMessage;
        }
    }
}