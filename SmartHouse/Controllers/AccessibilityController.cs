﻿using Microsoft.AspNetCore.Mvc;

namespace SmartHouse.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccessibilityController : ControllerBase
    {
        [HttpGet("GetHouseHistory")]
        public string GetHouseHistory(House _house)
        {
            var house =  _house;
            return house.GetHouseHistory();
        }

        [HttpPost("LockDownTheHouse")]
        public void LockDownTheHouse(House _house)
        {
            var house = _house;

            house.EnableFullLockDown();
        }

        [HttpPost("LockMainDoor")]
        public void LockMainDoor(House _house)
        {
            var house = _house;

            house.LockMainDoor();
        }

        [HttpPost("UnlockMainDoor")]
        public string UnlockMainDoor(decimal securityCode, House _house)
        {
            var house =  _house;

            return house.UnlockMainDoor(securityCode);
        }
    }
}