﻿using System.IO;

namespace SmartHouse
{
    public class House
    {
        private readonly Security _security;
        private readonly InformationService _informationService;
        private readonly FileDatabase db;

        public House(Security security,FileDatabase _fileDatabase,InformationService informationService)
        {
            _security = security;
            var fileDatabase = _fileDatabase;
            _informationService = informationService;
        }

        public void EnableFullLockDown( )
        {
            _security.FullLockDown(db);
        }

        public void LockMainDoor()
        {
            _security.LockFrontDoor(db);
        }

        public string UnlockMainDoor(decimal securityCode)
        {
            var security = new Security();

            return security.UnlockFrontDoor(securityCode,db);
        }

        public string GetHouseHistory()
        {
            return _informationService.GetInformation();
        }
    }
}