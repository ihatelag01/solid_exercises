﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CarsFilter.Enums;

namespace CarsFilter
{
    class ColorTypeFilter
    {
        public List<Car> FilterByType(IEnumerable<Car> cars, Color color,CarType type) =>
            cars.Where(c => c.Color == color && c.Type==type).ToList();
    }
}
