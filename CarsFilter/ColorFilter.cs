﻿using System.Collections.Generic;
using System.Linq;
using CarsFilter.Enums;

namespace CarsFilter
{
    public class ColorFilter
    {
         public List<Car> FilterByType(IEnumerable<Car> cars, Color color) =>
            cars.Where(c => c.Color == color).ToList();
    }
}